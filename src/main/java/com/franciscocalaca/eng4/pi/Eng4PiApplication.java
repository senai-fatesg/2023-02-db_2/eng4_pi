package com.franciscocalaca.eng4.pi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Eng4PiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Eng4PiApplication.class, args);
	}

}
